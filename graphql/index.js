const userResolvers = require('./users')
const messageResolvers = require('./messages')

module.exports = {
  Message: {
    // parent holds the value forwarded from the previous resolver
    createdAt: (parent) => parent.createdAt.toISOString(),
  },
  Query: {
    ...userResolvers.Query,
    ...messageResolvers.Query,
  },
  Mutation: {
    ...userResolvers.Mutation,
    ...messageResolvers.Mutation,
  },
}
